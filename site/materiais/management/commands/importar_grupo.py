# coding: utf-8

from django.core.management.base import BaseCommand, CommandError

from materiais.models import GrupoDeMaterial
import json


class Command(BaseCommand):
    args = 'caminho'
    
    def handle(self, *args, **options):
        caminho = args[0]
        json_data=open('grupos.json')
        data_grupos = json.load(caminho)
        for grupo in data_grupos:
            print 'Gravando' + grupo['nome'] + grupo['tipo']  
            grupoModel = GrupoDeMaterial()
            grupoModel.nome = grupo['nome']
            grupoModel.tipo = grupo['tipo'] 
            grupoModel.save()