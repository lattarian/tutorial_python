#coding: utf-8
from .forms import MaterialForm, MaterialModelForm
from .models import Material, GrupoDeMaterial
from django.shortcuts import render




def index(request):
    materiais = Material.objects.all()
    context = dict(nome="Colibri",
                   materiais=materiais )
    
    return render(request,'materiais/index.html', context)
    #return HttpResponse('Primeira view')
    
    
def detalhe(request, material_id):
    material = Material.objects.get(pk=int(material_id))
    context = dict(material=material)
    return render(request, 'materiais/detalhe.html', context)

def adicionar_plain(request):
    grupos = GrupoDeMaterial.objects.all()
    context = dict(
        grupos=grupos
    )
    if request.POST:
        material = Material()
        material.codigo = request.POST['codigo']
        material.descricao = request.POST['descricao']
        material.unidade = request.POST['unidade']
        material.grupo = GrupoDeMaterial.objects.get(pk=request.POST['grupo'])
        material.save()
        context['material'] = material

    return render(request, 'materiais/adicionar_plain.html', context)

def adicionar_class(request):
    context = dict()
    if request.POST:
        form=MaterialForm(request.POST)
        if form.is_valid():
            material = Material()
            material.codigo = form.cleaned_data['codigo']
            material.descricao = form.cleaned_data['descricao']
            material.unidade = form.cleaned_data['unidade']
            material.grupo = form.cleaned_data['grupo']
            material.save()
            context['material'] = material
    else:
        form=MaterialForm()

    context['form'] = form
    return render(request, 'materiais/adicionar_class.html', context)

def adicionar_model(request):
    context = dict()
    if request.POST:
        form = MaterialModelForm(request.POST)
        if form.is_valid():
            material = form.save()
            context['material'] = material
    else:
        form = MaterialModelForm()

    context['form'] = form
    return render(request, 'materiais/adicionar_class.html', context)




    