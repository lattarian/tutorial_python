#coding: utf-8

from .models import Material, GrupoDeMaterial
from django import forms

class MaterialModelForm(forms.ModelForm):
    class Meta:
        model = Material

class MaterialForm(forms.Form):
    codigo = forms.CharField(label=u'código', max_length=20)
    descricao = forms.CharField(label=u'descrição', max_length=100)
    unidade = forms.CharField(label=u'unidade', max_length=2)
    unidade = forms.CharField(label=u'unidade', max_length=2)
    grupo = forms.ModelChoiceField(
        label=u'grupo',
        queryset=GrupoDeMaterial.objects
    )
