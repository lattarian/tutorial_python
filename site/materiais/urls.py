#coding: utf-8
from . import views
from django.conf.urls import patterns, url


urlpatterns = patterns(
    '',
    url(r'^$', views.index, name='index'),
    url(r'^adicionar_plain', views.adicionar_plain, name="adicionar_plain"),
    url(r'^adicionar_class/$', views.adicionar_class, name='adicionar_class'),
    url(r'^adicionar_model/$', views.adicionar_model, name='adicionar_model'),
    url(r'^(?P<material_id>\d+)', views.detalhe, name="detalhe"),
    
)