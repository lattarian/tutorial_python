
Instalar Python e Fabric
------------------------

http://fgmacedo.com/programacao/instalando-o-python-e-o-fabric-no-windows/

Iniciar appliance Bitnami no VirtualBox
---------------------------------------

Manual `Quick Start`_.

.. _`Quick Start`: http://wiki.bitnami.com/Virtual_Appliances_Quick_Start_Guide#Virtual_Box

#) Crie uma nova máquina virtual e configure para Linux Ubuntu 64bit

#) Configure RAM para 512 ou maior.

#) Escolha "Utilizar um disco rígido virtual existente", e selecione o arquivo
   "vmdk" da máquina virtual BitNami.

#) A nossa rede corporativa não permite ingresso de novas máquinas no domínio,
   e o DHCP não fornece IPs para MAC ADDRESS não cadastrados. Deste modo,
   iremos configurar dois adaptadores de rede para contornar esta limitação,
   em outros ambientes você pode escolher "Bridged Adapter" para que a VM
   obtenha um IP na mesma rede da máquina host, e assim ter acesso a partir de
   outras máquinas em sua rede.

    Configure dois adaptadores:

    a) Adaptardor1: NAT
    b) Adaptardor2: "Host only Adapter", em avançado, selecione "Modo promíscuo".

#) Inicie a VM.

#) Faça login.

    O login e senha originais são:

        :login: bitnami
        :senha: bitnami

    Para o nosso estudo, mantenha a senha como *bitnami*. Em outros ambientes altere a senha.

#) Habilite acesso SSH

    Habilitar SSH na VM Bitnami::

         $ sudo mv /etc/init/ssh.conf.back /etc/init/ssh.conf
         $ sudo start ssh

    Para desabilitar, use o inverso::

         $ sudo stop ssh
         $ sudo mv /etc/init/ssh.conf /etc/init/ssh.conf.back


#) Habilite o segundo adaptador de rede

    A VM da Bitnami vem pré-configurada com um adaptador de rede **eth0**, vamos habilitar o segundo adaptador.

    Editar o arquivo /etc/network/interfaces::

        $ sudo vi /etc/network/interfaces

    O seu arquivo deve conter a configuração da rede eth0, adicione ao final do arquivo::

        auto eth1
        iface eth1 inet dhcp

#) Reinicie a VM.

#) Verifique se tudo está funcionando.

    #) VM com acesso à internet.

        No prompt da máquina virtual, digite::

            $ ping google.com

        Verifique se o ping foi bem sucedido.

    #) Host acessando a VM

        Descubra o IP da VM, O IP que estamos procurando começa com 192.
        Para descobrir o IP, no prompt da máquina virtual, digite::

            $ ifconfig


        Abra um prompt de comando na máquina host, e tente pingar a VM::

            ping <ip_da_vm>
